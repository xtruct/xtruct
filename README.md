![Xtruct logo](https://i.imgur.com/elo6RFS.png)

Do you feel disappointed about Construct 2/3 ?
Here's an interesting project for you.

Do you remember the time of Construct Classic ?
We're bringing back the similar experience of CC with modern libraries support.

## What is Xtruct?
Xtruct is a native (cross-platform) game editor with native desktop export.

## You're a developer
All you have to do is to grab your pants from the floor and step right in the development process to help us shape it

There is a lot of work to do to enable this engine to show its full potential, so we need you.

## You're a regular user
Feel free to ask questions, report bugs or even submit us your feature requests.
Also just saying *thanks* is very motivating. Don't forget to give us *a watch* 👁 and *a star* ⭐️

## This is an early Work In Progress project
Please keep that in mind